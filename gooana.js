/*
send			- fixed
event			- fixed
eventCategory	- Video
eventAction		- play
eventLabel		- name
*/

(function($){
    $.GooAna = function(el, options){
        // To avoid scope issues, use 'base' instead of 'this'
        // to reference this class from internal events and functions.
        var base = this;

        // Access to jQuery and DOM versions of element
        base.$el = $(el);
        base.el = el;

        // Add a reverse reference to the DOM object
        base.$el.data("GooAna", base);

        base.init = function(){
            // Put your initialization code here
            base.options = $.extend({},$.GooAna.defaultOptions, options);

            var employee_number = ($('meta[name="auth-empnum"]').attr('content') !== undefined) ? $('meta[name="auth-empnum"]').attr('content') : null;
            employee_number = (employee_number != null) ? '[' + employee_number + '] ' : '';

            ga('send', 'event', base.options.eventCategory, base.options.eventAction, employee_number + base.options.eventLabel);
        };

        // Sample Function, Uncomment to use
        // base.functionName = function(paramaters){
        //
        // };

        // Run initializer
        base.init();

    };

    $.GooAna.defaultOptions = {
        eventCategory: "",
        eventAction: "",
        eventLabel: ""
    };

    $.fn.gooAna = function(options){
        return this.each(function(){
            (new $.GooAna(this, options));
        });
    };

})(jQuery);

/*
 * Parameters: DOM Object, { eventcategory, eventAction, eventLabel } <-- provided by the Google Analytics
 * Example Code:
 * $.GooAna($(this), { eventCategory: "Products", eventAction: "Clicked the Wishlish Button", eventLabel: "Apple Iphone 4s" });
 */
